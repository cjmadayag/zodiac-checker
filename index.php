<!DOCTYPE html>
<html>
<head>
	<title>Zodiac</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/sandstone/bootstrap.css">
</head>
<body>
	<div class="d-flex justify-content-center align-items-center vh-100 flex-column">
		<h1 class="">Welcome to Zodiac Checker</h1>
		<form action="controllers/zodiac-process.php" class="bg-info rounded p-5" method="POST">
			<div class="form-group">
				<label>Name:</label>
				<input type="text" name="name" class="form-control">
			</div>
			<div class="form-group">
				<label>Birth Month:</label>
				<input type="number" name="birthMonth" class="form-control">
			</div>
			<div class="form-group">
				<label>Birth Day:</label>
				<input type="number" name="birthDay" class="form-control">
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-success">Check Zodiac</button>
			</div>
			<?php session_start(); 
				if(isset($_SESSION["errorMsg"])){
			?>
			
			<p class="text-center mt-3">
				<?php
					echo $_SESSION["errorMsg"];
					session_destroy();
				 ?>
			</p>	 
			<?php 
			}
			?>
			
		</form>
	</div>
	
</body>
</html>