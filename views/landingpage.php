<!DOCTYPE html>
<html>
<head>
	<title>Zodiac</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/sandstone/bootstrap.css">
</head>
<body>
	<?php session_start(); ?>
	<div class="d-flex justify-content-center align-items-center vh-100">
		<div class="text-center">
			<h1>Hello <?php echo $_SESSION["name"]?></h1>
			<h1>You are <?php echo $_SESSION["zodiac"];
			session_destroy();
			?></h1>
		</div>
	</div>
</body>
</html>