<?php 
	session_start();
	$_SESSION["name"] = $_POST["name"];
	$_SESSION["birthMonth"] = $_POST["birthMonth"];
	$_SESSION["birthDay"] = $_POST["birthDay"];
	
	if($_SESSION["birthMonth"]>=1 && $_SESSION["birthMonth"]<=12 && $_SESSION["birthDay"]>=1 && $_SESSION["birthDay"]<=31){
		switch($_SESSION["birthMonth"]){
			case 1:
				if($_SESSION["birthDay"] >= 1 && $_SESSION["birthDay"] <= 19){
					$zodiac="Capricorn";
				}
				else if($_SESSION["birthDay"] <= 31){
					$zodiac="Aquarius";
				}
				else{
					$_SESSION["errorMsg"] = "Please enter correct date";
					header("Location: ". $_SERVER["HTTP_REFERER"]);
				}
				break;
			case 2:
				if($_SESSION["birthDay"] >= 1 && $_SESSION["birthDay"] <= 18){
					$zodiac="Aquarius";
				}
				else if($_SESSION["birthDay"] <= 29){
					$zodiac="Pisces";
				}
				else{
					$_SESSION["errorMsg"] = "Please enter correct date";
					header("Location: ". $_SERVER["HTTP_REFERER"]);
				}
				break;
			case 3:
				if($_SESSION["birthDay"] >= 1 && $_SESSION["birthDay"] <= 20){
					$zodiac="Pisces";
				}
				else if($_SESSION["birthDay"] <= 31){
					$zodiac="Aries";
				}
				else{
					$_SESSION["errorMsg"] = "Please enter correct date";
					header("Location: ". $_SERVER["HTTP_REFERER"]);
				}
				break;
			case 4:
				if($_SESSION["birthDay"] >= 1 && $_SESSION["birthDay"] <= 19){
					$zodiac="Aries";
				}
				else if($_SESSION["birthDay"] <= 30){
					$zodiac="Taurus";
				}
				else{
					$_SESSION["errorMsg"] = "Please enter correct date";
					header("Location: ". $_SERVER["HTTP_REFERER"]);
				}
				break;
			case 5:
				if($_SESSION["birthDay"] >= 1 && $_SESSION["birthDay"] <= 20){
					$zodiac="Taurus";
				}
				else if($_SESSION["birthDay"] <= 31){
					$zodiac="Gemini";
				}
				else{
					$_SESSION["errorMsg"] = "Please enter correct date";
					header("Location: ". $_SERVER["HTTP_REFERER"]);
				}
				break;
			case 6:
				if($_SESSION["birthDay"] >= 1 && $_SESSION["birthDay"] <= 20){
					$zodiac="Gemini";
				}
				else if($_SESSION["birthDay"] <= 30){
					$zodiac="Cancer";
				}
				else{
					$_SESSION["errorMsg"] = "Please enter correct date";
					header("Location: ". $_SERVER["HTTP_REFERER"]);
				}
				break;
			case 7:
				if($_SESSION["birthDay"] >= 1 && $_SESSION["birthDay"] <= 22){
					$zodiac="Cancer";
				}
				else if($_SESSION["birthDay"] <= 31){
					$zodiac="leo";
				}
				else{
					$_SESSION["errorMsg"] = "Please enter correct date";
					header("Location: ". $_SERVER["HTTP_REFERER"]);
				}
				break;
			case 8:
				if($_SESSION["birthDay"] >= 1 && $_SESSION["birthDay"] <= 22){
					$zodiac="leo";
				}
				else if($_SESSION["birthDay"] <= 31){
					$zodiac="Virgo";
				}
				else{
					$_SESSION["errorMsg"] = "Please enter correct date";
					header("Location: ". $_SERVER["HTTP_REFERER"]);
				}
				break;
			case 9:
				if($_SESSION["birthDay"] >= 1 && $_SESSION["birthDay"] <= 22){
					$zodiac="Virgo";
				}
				else if($_SESSION["birthDay"] <= 30){
					$zodiac="Libra";
				}
				else{
					$_SESSION["errorMsg"] = "Please enter correct date";
					header("Location: ". $_SERVER["HTTP_REFERER"]);
				}
				break;
			case 10:
				if($_SESSION["birthDay"] >= 1 && $_SESSION["birthDay"] <= 22){
					$zodiac="Libra";
				}
				else if($_SESSION["birthDay"] <= 31){
					$zodiac="Scorpio";
				}
				else{
					$_SESSION["errorMsg"] = "Please enter correct date";
					header("Location: ". $_SERVER["HTTP_REFERER"]);
				}
				break;
			case 11:
				if($_SESSION["birthDay"] >= 1 && $_SESSION["birthDay"] <= 21){
					$zodiac="Scorpio";
				}
				else if($_SESSION["birthDay"] <= 30){
					$zodiac="Sagittarius";
				}
				else{
					$_SESSION["errorMsg"] = "Please enter correct date";
					header("Location: ". $_SERVER["HTTP_REFERER"]);
				}
				break;
			case 12:
				if($_SESSION["birthDay"] >= 1 && $_SESSION["birthDay"] <= 21){
					$zodiac="Sagittarius";
				}
				else if($_SESSION["birthDay"] <= 31){
					$zodiac="Capricorn";
				}
				else{
					$_SESSION["errorMsg"] = "Please enter correct date";
					header("Location: ". $_SERVER["HTTP_REFERER"]);
				}
				break;
		}
		if(isset($zodiac)){
			$_SESSION["zodiac"] = $zodiac;
			header("Location: ../views/landingpage.php");
		}
	}
	else{
		$_SESSION["errorMsg"] = "Please complete the form";
		header("Location: ". $_SERVER["HTTP_REFERER"]);
	}
 ?>